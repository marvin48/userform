package com.example.userform;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserDAO {

    //Create
    @Insert
    void insertarUsuario(User miUsuario);

    //Retrieve
    @Query("SELECT * FROM user")
    List<User> obtenerTodos();

    @Query("SELECT * FROM user WHERE uid=:id")
    User obtenerUsuario(int id);

    //UPDATE
    @Update
    void actualizarUsario(User miUsuario);




}
