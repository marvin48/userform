package com.example.userform;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class DBList extends AppCompatActivity {

    RecyclerView miVistaRecicladora;
    UserAdapter miAdaptador;
    UserDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dblist);

        miVistaRecicladora=findViewById(R.id.listaUsuarios);
        db=UserDatabase.getDatabase(this);

        Bundle misDatos=getIntent().getExtras();

        User miUsuario=new User();
        miUsuario.nombre=misDatos.getString("nombre");
        miUsuario.apellido=misDatos.getString("apellido");
        miUsuario.peso=Float.parseFloat(misDatos.getString("peso"));

        //Cargar los datos empleando el DAO
        //Tenemos que crear un nuevo hilo de ejecución
        new Thread(
                ()->{
                    db.userDao().insertarUsuario(miUsuario);
                }
        ).start();

        //Obtener los registros de la tabla
        new Thread(
                ()->{
                    List<User> misUsuarios = db.userDao().obtenerTodos();
                    miAdaptador = new UserAdapter(misUsuarios);

                    runOnUiThread(
                            ()->{
                                miVistaRecicladora.setAdapter(miAdaptador);
                                miVistaRecicladora.setLayoutManager(new LinearLayoutManager(this));
                            }
                    );
                }
                ).start();


    }
}